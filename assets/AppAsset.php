<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'css/default.css',
		'css/layout.css',
		'css/media-queries.css',
		'css/magnific-popup.css',
        //'css/site.css',
		//'css/styles.css'
    ];
    public $js = [
    	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
    	'js/jquery-migrate-1.2.1.min.js',
		'js/modernizr.js',
		//'js/jquery.flexslider.js',
		//'js/waypoints.js',
		//'js/jquery.fittext.js',
		'js/magnific-popup.js',
		'js/init.js',
		'js/scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
		//'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
