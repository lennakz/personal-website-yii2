<?php

use yii\db\Migration;

/**
 * Handles the creation of table `experience`.
 */
class m181129_204338_create_experience_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('experience', [
            'id' => $this->primaryKey(),
			'user' => $this->integer(),
			'date_start' => $this->date(),
			'date_end' => $this->date(),
			'company_name' => $this->string(),
			'position' => $this->string(),
			'description' => $this->text(),
			'skills' => $this->string(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('experience');
    }
}
