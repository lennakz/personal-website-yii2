<?php

use yii\db\Migration;

/**
 * Handles the creation of table `references`.
 */
class m181129_214822_create_references_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('references', [
            'id' => $this->primaryKey(),
			'user' => $this->integer(),
			'first_name' => $this->string(),
			'last_name' => $this->string(),
			'organization' => $this->string(),
			'position' => $this->string(),
			'phone' => $this->string(),
			'email' => $this->string(),
			'qoute' => $this->text(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('references');
    }
}
