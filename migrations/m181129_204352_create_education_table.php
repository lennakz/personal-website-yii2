<?php

use yii\db\Migration;

/**
 * Handles the creation of table `education`.
 */
class m181129_204352_create_education_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('education', [
            'id' => $this->primaryKey(),
			'user' => $this->integer(),
			'date_start' => $this->date(),
			'date_end' => $this->date(),
			'institute_name' => $this->string(),
			'institute_logo' => $this->string(),
			'institute_link' => $this->string(),
			'skills' => $this->string(),
			'certificate' => $this->string(),
			'description' => $this->text(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('education');
    }
}
