<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181129_211816_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
			'first_name' => $this->string(),
			'last_name' => $this->string(),
			'photo_url' => $this->string(),
			'resume_url' => $this->string(),
			'short_description' => $this->text(),
			'phone' => $this->string(),
			'email' => $this->string(),
			'facebook' => $this->string(),
			'twitter' => $this->string(),
			'instagram' => $this->string(),
			'linkedin' => $this->string(),
			'codepen' => $this->string(),
			'github' => $this->string(),
			'gitlab' => $this->string(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
