<section id="about">

	<div class="row">

		<div class="three columns">
            <img class="profile-pic"  src="{$user->getProfileImage()}" alt="{$user->getFullName()|escape}">
		</div>

		<div class="nine columns main-col">

            <h2>About Me</h2>

            <p>{$user->getDescription()|escape}</p>

			<ul class="social">
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
				<li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>

			{if $user->hasResume()}
				<div class="row">
					<div class="columns download">
						<p>
							<a href="{$user->getResumeUrl()}" class="button"><i class="fa fa-download"></i>Download Resume</a>
						</p>
					</div>
				</div>
			{/if}

		</div>

	</div>

</section>

<section id="portfolio">
	<div class="row">
		<div class="twelve columns collapsed">
			<h1>Check Out Some of My Works.</h1>
			<div id="portfolio-wrapper" class="bgrid-quarters s-bgrid-thirds cf">

				{foreach range(1, 8) as $i}
					<div class="columns portfolio-item">
						<div class="item-wrap">
							<a href="#modal-{$i}" title="">
								<img alt="" src="images/portfolio/coffee.jpg">
								<div class="overlay">
									<div class="portfolio-item-meta">
										<h5>Coffee</h5>
										<p>Illustrration</p>
									</div>
								</div>
								<div class="link-icon"><i class="icon-plus"></i></div>
							</a>
						</div>
					</div>
				{/foreach}

			</div>
		</div>

		{foreach range(1, 8) as $i}
			<div id="modal-{$i}" class="popup-modal mfp-hide">
				<img class="scale-with-grid" src="images/portfolio/modals/m-coffee.jpg" alt="" />
				<div class="description-box">
					<h4>Coffee Cup</h4>
					<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
					<span class="categories"><i class="fa fa-tag"></i>Branding, Webdesign</span>
				</div>
				<div class="link-box">
					<a href="http://www.behance.net">Details</a>
					<a class="popup-modal-dismiss">Close</a>
				</div>
			</div>
		{/foreach}

	</div>
</section>

<section id="resume">

	<div class="row education">
		<div class="three columns header-col">
			<h1><span>Education</span></h1>
		</div>
		<div class="nine columns main-col">

			{foreach $user->education as $m}
				<div class="row item">
					<div class="twelve columns">
						<h3>{$m->institute_name|escape}</h3>
						<p class="info">{$m->certificate|escape} <span>&bull;</span> <em class="date">{$m->getDatesString()|escape}</em></p>
						<p>{$m->description|escape}</p>
						<p>{$m->renderSkillsEarned()}</p>
					</div>
				</div>
			{/foreach}

		</div>
	</div>

	<div class="row work">
		<div class="three columns header-col">
			<h1><span>Work</span></h1>
		</div>
		<div class="nine columns main-col">

			{foreach $user->experience as $m}
				<div class="row item">
					<div class="twelve columns">
						<h3>{$m->company_name|escape}</h3>
						<p class="info">{$m->position} <span>&bull;</span> <em class="date">{$m->getDatesString()|escape}</em></p>
						<p>{$m->description|escape}</p>
						<p>{$m->renderSkillsEarned()}</p>
					</div>
				</div>
			{/foreach}

		</div>
	</div>

	<div class="row skill">
		<div class="three columns header-col">
			<h1><span>Skills</span></h1>
		</div>
		<div class="nine columns main-col">
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
				eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
				voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
				voluptatem sequi nesciunt.
			</p>

			<div class="bars">

				<ul class="skills">
					<li><span class="bar-expand photoshop"></span><em>Photoshop</em></li>
					<li><span class="bar-expand illustrator"></span><em>Illustrator</em></li>
					<li><span class="bar-expand wordpress"></span><em>Wordpress</em></li>
					<li><span class="bar-expand css"></span><em>CSS</em></li>
					<li><span class="bar-expand html5"></span><em>HTML5</em></li>
					<li><span class="bar-expand jquery"></span><em>jQuery</em></li>
				</ul>

			</div>

		</div>

	</div>

</section>

<section id="contact">
	<div class="row section-head">
		<div class="two columns header-col">
			<h1><span>Get In Touch.</span></h1>
		</div>
		<div class="ten columns">
			<p class="lead">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
				eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
				voluptatem quia voluptas sit aspernatur aut odit aut fugit.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="eight columns">
			<!-- form -->
			<form action="" method="post" id="contactForm" name="contactForm">
				<fieldset>
					<div>
						<label for="contactName">Name <span class="required">*</span></label>
						<input type="text" value="" size="35" id="contactName" name="contactName">
					</div>

					<div>
						<label for="contactEmail">Email <span class="required">*</span></label>
						<input type="text" value="" size="35" id="contactEmail" name="contactEmail">
					</div>

					<div>
						<label for="contactSubject">Subject</label>
						<input type="text" value="" size="35" id="contactSubject" name="contactSubject">
					</div>

					<div>
						<label for="contactMessage">Message <span class="required">*</span></label>
						<textarea cols="50" rows="15" id="contactMessage" name="contactMessage"></textarea>
					</div>

					<div>
						<button class="submit">Submit</button>
						<span id="image-loader">
							<img alt="" src="images/loader.gif">
						</span>
					</div>
				</fieldset>
			</form>
			<div id="message-warning"> Error boy</div>
			<div id="message-success">
				<i class="fa fa-check"></i>Your message was sent, thank you!<br>
			</div>
		</div>

		<aside class="four columns footer-widgets">
			<div class="widget widget_contact">
				<h4>Address and Phone</h4>
				<p class="address">
					Jonathan Doe<br>
					1600 Amphitheatre Parkway <br>
					Mountain View, CA 94043 US<br>
					<span>(123) 456-7890</span>
				</p>
			</div>

			<div class="widget widget_tweets">
				<h4 class="widget-title">Latest Tweets</h4>
				<ul id="twitter">
					<li>
                        <span>
							This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
							Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
							<a href="#">http://t.co/CGIrdxIlI3</a>
                        </span>
                        <b><a href="#">2 Days Ago</a></b>
					</li>
					<li>
                        <span>
							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
							eaque ipsa quae ab illo inventore veritatis et quasi
							<a href="#">http://t.co/CGIrdxIlI3</a>
                        </span>
                        <b><a href="#">3 Days Ago</a></b>
					</li>
				</ul>
			</div>
		</aside>

	</div>

</section>