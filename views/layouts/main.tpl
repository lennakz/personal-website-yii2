{$this->beginPage()}
<!DOCTYPE html>
<html lang="{$app->language}">
	<head>
		<meta charset="{$app->charset}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		{\yii\helpers\Html::csrfMetaTags()}

		<title>{$this->PageTitle|escape}</title>

		{$this->head()}

		<link rel="shortcut icon" href="favicon.ico">
	</head>
	<body>
		{$this->beginBody()}

		{partial view='/partials/header'}

		{$content}

		{partial view='/partials/footer'}

		{$this->endBody()}
	</body>
</html>
{$this->endPage()}
