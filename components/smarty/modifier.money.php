<?php

function smarty_modifier_money($value)
{
    if ($value < 0)
		return '-$' . number_format(abs($value), 2);
	else
		return '$' . number_format($value, 2);
}