<?php

function smarty_function_img($params, &$smarty)
{
	if (empty($params['src']))
		throw new yii\web\ServerErrorHttpException("You need to specify the src parameter.");

	$src = $params['src'];
	unset($params['src']);
	
	if (substr($src, 0, 7) === '/theme/')
	{
		$path = \Yii::$app->assetManager->getPublishedUrl('@webdir/theme');
		
		if ($path !== false)
			$src = str_replace('/theme/', $path . '/', $src);
	}
	
	return \yii\helpers\Html::img($src, $params);
}
