<?php

function smarty_modifier_phone($value)
{
	$phone = str_replace(array('(', ')', '-', ' ', '.'), '', $value);

	if (strlen($phone) === 10)
		return '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6, 4);
	if (strlen($phone) === 11)
		return substr($phone, 0, 1) . ' (' . substr($phone, 1, 3) . ') ' . substr($phone, 4, 3) . '-' . substr($phone, 7, 4);

	return $value;
}