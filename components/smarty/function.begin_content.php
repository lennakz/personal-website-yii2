<?php

function smarty_function_begin_content($params, &$smarty)
{
    if (empty($params['view']))
        throw new yii\web\ServerErrorHttpException("You need to specify the view parameter.");
	
	$view = $params['view'];
	unset($params['view']);

	$smarty->tpl_vars['this']->value->beginContent($view, $params);
	return '';
}