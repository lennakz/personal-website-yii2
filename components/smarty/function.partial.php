<?php

function smarty_function_partial($params, &$smarty)
{
    if (empty($params['view']))
        throw new yii\web\ServerErrorHttpException("You need to specify the view parameter.");
	
	$view = $params['view'];
	unset($params['view']);
	
	/* @var $renderer app\common\View */
	$renderer = $smarty->tpl_vars['this']->value;

    return $renderer->render($view, $params);
}