<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 30/11/18
 * Time: 3:35 PM
 */

namespace app\components;


trait HistoryTrait
{
	public function getDatesString()
	{
		$date_end = (empty($this->date_end) ? 'Current' : date('F Y', strtotime($this->date_end)));
		
		return date('F Y', strtotime($this->date_start)) . ' - ' . $date_end;
	}
	
	public function renderSkillsEarned()
	{
		if (empty($this->skills))
			return;
	}
}