<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $photo_url
 * @property string $resume_url
 * @property string $short_description
 * @property string $phone
 * @property string $email
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $linkedin
 * @property string $codepen
 * @property string $github
 * @property string $gitlab
 * @property string $updated_at
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord
{
	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short_description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['first_name', 'last_name', 'photo_url', 'resume_url', 'phone', 'email', 'facebook', 'twitter', 'instagram', 'linkedin', 'codepen', 'github', 'gitlab'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'photo_url' => 'Photo Url',
            'resume_url' => 'Resume Url',
            'short_description' => 'Short Description',
            'phone' => 'Phone',
            'email' => 'Email',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'linkedin' => 'Linkedin',
            'codepen' => 'Codepen',
            'github' => 'Github',
            'gitlab' => 'Gitlab',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
	
	public function getEducation()
	{
		return $this->hasMany(Education::class, ['user' => 'id'])->orderBy('date_start DESC');
    }
	
	public function getExperience()
	{
		return $this->hasMany(Experience::class, ['user' => 'id'])->orderBy('date_start DESC');
	}
	
	public function getProjects()
	{
		return $this->hasMany(Project::class, ['user' => 'id']);
	}
	
	public function getSkills()
	{
		return $this->hasMany(Skill::class, ['user' => 'id']);
	}
	
	public function getFullName()
	{
		return "{$this->first_name} {$this->last_name}";
	}
	
	public function getDescription()
	{
		return $this->short_description;
	}
	
	public function getProfileImage()
	{
		$url = 'images/users/' . $this->id . '/' . $this->photo_url;
		if (!empty($this->photo_url) and file_exists($url))
			return $url;

		return 'images/profilepic.jpg';
	}
	
	public function hasResume()
	{
		return (!empty($this->resume_url) and file_exists($this->getResumeUrl()));
	}
	
	public function getResumeUrl()
	{
		return 'files/users/' . $this->id . '/' . $this->resume_url;
	}
	
}
