<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "skills".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $projects
 * @property string $icon
 * @property int $level
 * @property string $description
 * @property string $updated_at
 * @property string $created_at
 */
class Skill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skills';
    }

    public static function typesMap()
	{
		return [
			0 => 'No type',
			1 => 'Technical',
			2 => 'Pesonal',
		];
	}
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'type', 'level'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'projects', 'icon', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'name' => 'Name',
            'type' => 'Type',
            'projects' => 'Projects',
            'icon' => 'Icon',
            'level' => 'Level',
            'description' => 'Description',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
	
	public function getType()
	{
		$types = self::typesMap();
		
		return $types[$this->type];
    }
}
