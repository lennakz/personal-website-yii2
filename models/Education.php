<?php

namespace app\models;

use app\components\HistoryTrait;
use Yii;

/**
 * This is the model class for table "education".
 *
 * @property int $id
 * @property int $user
 * @property string $date_start
 * @property string $date_end
 * @property string $institute_name
 * @property string $institute_logo
 * @property string $institute_link
 * @property string $skills
 * @property string $certificate
 * @property string $description
 * @property string $updated_at
 * @property string $created_at
 */
class Education extends \yii\db\ActiveRecord
{
	use HistoryTrait;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user'], 'integer'],
            [['date_start', 'date_end', 'updated_at', 'created_at'], 'safe'],
            [['description'], 'string'],
            [['institute_name', 'institute_logo', 'institute_link', 'skills', 'certificate'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'institute_name' => 'Institute Name',
            'institute_logo' => 'Institute Logo',
            'institute_link' => 'Institute Link',
            'skills' => 'Skills',
            'certificate' => 'Certificate',
            'description' => 'Description',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
    
}
