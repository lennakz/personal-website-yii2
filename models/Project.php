<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $user
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $link
 * @property string $source_link
 * @property string $skills
 * @property string $updated_at
 * @property string $created_at
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'image', 'description', 'link', 'source_link', 'skills'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'name' => 'Name',
            'image' => 'Image',
            'description' => 'Description',
            'link' => 'Link',
            'source_link' => 'Source Link',
            'skills' => 'Skills',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
