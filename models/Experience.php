<?php

namespace app\models;

use app\components\HistoryTrait;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "experience".
 *
 * @property int $id
 * @property int $user
 * @property string $date_start
 * @property string $date_end
 * @property string $company_name
 * @property string $position
 * @property string $description
 * @property string $skills
 * @property string $updated_at
 * @property string $created_at
 */
class Experience extends \yii\db\ActiveRecord
{
	use HistoryTrait;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user'], 'integer'],
            [['date_start', 'date_end', 'updated_at', 'created_at'], 'safe'],
            [['description'], 'string'],
            [['company_name', 'position', 'skills'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'company_name' => 'Company Name',
            'position' => 'Position',
            'description' => 'Description',
            'skills' => 'Skills',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
	
	public function getSkills()
	{
		if (empty($this->skills))
			return new ActiveQuery();
		
		$skills = explode(',', $this->skills);
		
		return Skill::find()->where(['id' => $skills]);
    }
    
}
