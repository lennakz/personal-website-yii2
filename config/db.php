<?php

$config = require __DIR__ . '/env.php';

return [
	'class' => 'yii\db\Connection',
	'dsn' => "mysql:host={$config['HOST']};dbname={$config['DBNAME']}",
	'username' => $config['USERNAME'],
	'password' => $config['PASSWORD'],
	'charset' => 'utf8',
	// Schema cache options (for production environment)
	//'enableSchemaCache' => true,
	//'schemaCacheDuration' => 60,
	//'schemaCache' => 'cache',
];
